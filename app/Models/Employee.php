<?php

namespace App\Models;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
     use HasApiTokens, HasFactory;

     protected $fillable = [
        'name', 'age', 'job', 'salary'
    ];
}