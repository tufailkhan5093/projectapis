<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
class UserAuthController extends Controller
{
    public function register(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
            'dob'=>'required',
        ]);

        $data['password'] = bcrypt($request->password);
        $data['dob']=$request->dob;
        $user = User::create($data);
        $token = $user->createToken('Ma Nahi Btao Ga')->accessToken;

        return response()->json($token,201);
    }

    public function login(Request $request)
    {
        $data = $request->validate([
            'email' => 'email|required',
            'password' => 'required'
        ]);

        if (!auth()->attempt($data)) {
            return response(['error_message' => 'Incorrect Details. 
            Please try again']);
        }

        $token = auth()->user()->createToken('Ma Nahi Btao Ga')->accessToken;
        $response = ['token' => $token];
        return response()->json($response, 201);
        //return response(['user' => auth()->user(), 'token' => $token]);

    }
}